package com.example.andrei.routetracker;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrei.routetracker.models.Route;
import com.example.andrei.routetracker.strategies.RouteListAdapter;
import com.example.andrei.routetracker.strategies.LoginStrategy;
import com.example.andrei.routetracker.strategies.MongoStrategy;

import java.util.ArrayList;

public class HomeActivity extends ToolbarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView tv2 = (TextView) findViewById(R.id.textView2);
        ListView routesList = (ListView) findViewById(R.id.lvRoutes);

        if(MyGlobalApp.getCurrentUser()==null){
            Intent it = new Intent(this,SigninActivity.class);
            Toast.makeText(getApplicationContext(),"No user found",Toast.LENGTH_LONG).show();

            startActivity(it);

        }
        tv2.append(" " + MyGlobalApp.getCurrentUser().getUsername());

        //Filters for the list view
        EditText searchByTitle = (EditText) findViewById(R.id.et_searchByTitle);



        // Creating a button - Load More
        Button btnLoadMore = new Button(this);
        btnLoadMore.setText(R.string.load_more);


        // Adding button to listview at footer
        routesList.addFooterView(btnLoadMore);

        //Getting the routes and setting adapter
        ArrayList<Route> routes = MongoStrategy.getInstance().getAllRoutes();
        //Number of routes to be shown on list
        final int NR_ROUTES_ON_LIST = 4;
        ArrayList<Route> visibleRoutes = new ArrayList<>();
        for (int i = 0; i < NR_ROUTES_ON_LIST && i < routes.size(); i++) {
            visibleRoutes.add(routes.get(i));
        }

        //Custom adapter
        RouteListAdapter routeListAdapter = new RouteListAdapter(this, visibleRoutes);

        //TODO: Adauga ratings pe lista. Trebe sa iei reviews de pe baza de date.
        //TO DO: Trebe creat custom adapter care sa aiba filtru si paginatie sau loadMore. Daca se ajunge sa coste prea mult dpdv al timpului sa iau toate datele. Sa incerc sa le iau pe rand.
        if (!visibleRoutes.isEmpty()) {

            routesList.setAdapter(routeListAdapter);
        } else {
            tv2.append(" \nThere are no routes to be shown??");
        }

        searchByTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                routeListAdapter.filterTitle(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (visibleRoutes.size() < routes.size()) {
                    //Se mareste size in interirorul loop-ului
                    int howMuch = visibleRoutes.size() + NR_ROUTES_ON_LIST;
                    for (int i = visibleRoutes.size(); i < howMuch && i < routes.size(); i++) {
                        visibleRoutes.add(routes.get(i));
                    }
                    // get listview current position - used to maintain scroll position
                    int currentPosition = routesList.getFirstVisiblePosition();

                    // Appending new data to menuItems ArrayList
                    routesList.setAdapter(new RouteListAdapter(HomeActivity.this, visibleRoutes));
                    // Setting new scroll position
                    routesList.setSelectionFromTop(currentPosition + 1, 0);

                } else {
                    Toast.makeText(getApplicationContext(), "No more routes to be shown", Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}


