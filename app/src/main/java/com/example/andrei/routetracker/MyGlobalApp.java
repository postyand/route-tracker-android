package com.example.andrei.routetracker;

import android.app.Application;
import android.content.Context;

import com.example.andrei.routetracker.models.User;

/**
 * Created by Andrei on 4/16/2017.
 */

public class MyGlobalApp extends Application {

    public static final String MyPREFERENCES = "SvdPrf";
    public static boolean offlineMode = false;
    private static User currentUser;

    public void onCreate() {
        super.onCreate();
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        MyGlobalApp.currentUser = currentUser;
    }
}
