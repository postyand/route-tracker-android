package com.example.andrei.routetracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.andrei.routetracker.models.User;
import com.example.andrei.routetracker.strategies.LoginStrategy;
import com.example.andrei.routetracker.strategies.MongoStrategy;
import com.mapbox.mapboxsdk.Mapbox;

import org.bson.types.ObjectId;

import static com.example.andrei.routetracker.MyGlobalApp.MyPREFERENCES;

public class BufferActivity extends AppCompatActivity {


    /*
 * isOnline - Check if there is a NetworkConnection
 * @return boolean
 */
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return  (netInfo != null && netInfo.isConnected());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buffer);

        Mapbox.getInstance(getApplicationContext(),getString(R.string.mapbox_access_token) );

        Intent intent;
        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        if(isOnline()) {
            //connect to database
            MongoStrategy.getInstance();

            if (sharedpreferences.contains("storedUsername")) {
                Log.d("Buffer SP cont", sharedpreferences.getString("storedUsername", "gresit username(pref)"));
                User storedUser = new User(sharedpreferences.getString("storedUsername", "gresit username(pref)"),
                        sharedpreferences.getString("storedPassword", "gresit parola(pref)"),
                        new ObjectId(sharedpreferences.getString("storedUserId",new ObjectId().toString())));


                MyGlobalApp.setCurrentUser(storedUser);

                intent = new Intent(getApplicationContext(), HomeActivity.class);
            } else {

                Log.d("Buffer SP not cont", "nu contine");
                intent = new Intent(getApplicationContext(), SigninActivity.class);
            }
        }
        else{
            Log.i("Buffer SP offline","trebe sa fac ceva pe offline");
            intent = new Intent(getApplicationContext(), OfflineMapActivity.class);
        }

        Log.d("Buffer intent", intent.toString());
        startActivity(intent);
        finish();
    }
}
