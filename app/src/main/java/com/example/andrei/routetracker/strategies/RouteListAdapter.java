package com.example.andrei.routetracker.strategies;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.andrei.routetracker.MapActivity;
import com.example.andrei.routetracker.R;
import com.example.andrei.routetracker.models.Route;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Andrei on 5/2/2017.
 */

public class RouteListAdapter extends ArrayAdapter<Route> implements Filterable {
    private final Context context;
    private ArrayList<Route> values=null;
    private ArrayList<Route> filteredValues=null;
    private  TitleFilter titleFilter = new TitleFilter();

    public RouteListAdapter(Context context, ArrayList<Route> values) {
        super(context, R.layout.list_item_routes, values);
        this.context = context;
        this.values = values;
        this.filteredValues=values;
    }

    public int getCount() {
        return filteredValues.size();
    }

    public Route getItem(int position) {
        return filteredValues.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void filterTitle(CharSequence title){
        titleFilter.filter(title);
    }

    private class TitleFilter extends Filter {



        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if(constraint == null || constraint.length() == 0){
                result.values = values;
                result.count = values.size();
            }else{
                ArrayList<Route> filteredList = new ArrayList<>();
                for(Route r: values){
                    if(r.getTitle().contains(constraint))
                        filteredList.add(r);
                }
                result.values = filteredList;
                result.count = filteredList.size();
            }

            return result;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0) {
                notifyDataSetInvalidated();
            } else {
                filteredValues = (ArrayList<Route>) results.values;
                notifyDataSetChanged();
            }
        }

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MapActivity.class);
                intent.putExtra("EXTRA_SELECTED_ROUTE",filteredValues.get(position));
                context.startActivity(intent);
            }
        };



        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_item_routes, parent, false);

            holder = new ViewHolder();
            holder.tvCost = (TextView) vi.findViewById(R.id.tv_routeCost);
            holder.tvDuration = (TextView) vi.findViewById(R.id.tv_routeDuration);
            holder.tvStart = (TextView) vi.findViewById(R.id.tv_routeStart);
            holder.tvFinish = (TextView) vi.findViewById(R.id.tv_routeFinish);
            holder.tvTitle = (TextView) vi.findViewById(R.id.tv_routeTitle);


            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();


        holder.tvTitle.setText(filteredValues.get(position).getTitle());

        holder.tvStart.setText(filteredValues.get(position).getStart().getName());
        holder.tvFinish.setText(filteredValues.get(position).getDestination().getName());

        if (filteredValues.get(position).getCost() == -1) {
            holder.tvCost.setText(R.string.cost_not_specified);
        } else
            holder.tvCost.setText(String.format(Locale.getDefault(), "€ %.0f", filteredValues.get(position).getCost()));

        if (filteredValues.get(position).getTime() == -1) {
            holder.tvDuration.setText(R.string.not_specified);
        } else
            holder.tvDuration.setText(String.format(Locale.getDefault(), "%d days", filteredValues.get(position).getTime()));

        //TODO:Cod pentru rating

        vi.setOnClickListener(clickListener);
        return vi;

    }

    private class ViewHolder {

        TextView tvCost;
        TextView tvDuration;
        TextView tvStart;
        TextView tvFinish;
        TextView tvTitle;

    }
}


