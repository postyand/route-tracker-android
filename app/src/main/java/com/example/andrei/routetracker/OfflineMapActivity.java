package com.example.andrei.routetracker;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.andrei.routetracker.models.Point;
import com.example.andrei.routetracker.models.Route;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;
import com.mapbox.services.Constants;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import org.json.JSONObject;

import java.util.List;

import static com.example.andrei.routetracker.MapActivity.JSON_CHARSET;

public class OfflineMapActivity extends ToolbarActivity {

    private MapView mapView;
    private OfflineManager offlineManager;
    private Route route;
    private DirectionsRoute generatedRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_map);

        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));

        // This contains the MapView in XML and needs to be called after the access token is configured.
        setContentView(R.layout.activity_offline_map);

        mapView = (MapView) findViewById(R.id.mapbox_map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                // Set up the OfflineManager
                offlineManager = OfflineManager.getInstance(OfflineMapActivity.this);

                //check if there is an offline region
                offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
                    @Override
                    public void onList(OfflineRegion[] offlineRegions) {
                        if (offlineRegions.length > 0) {
                            //Get the region
                            OfflineRegion offlineRegion = offlineRegions[0];
                            //there is an offline region, get the metadata
                            byte[] metadata = offlineRegion.getMetadata();
                            try {
                                String jsonString = new String(metadata, JSON_CHARSET);
                                JSONObject jsonObject = new JSONObject(jsonString);
                                Gson gson = new Gson();
                                //TODO: This is just a string.

                                //Get route details
                                String routeString = jsonObject.getString("route");
                                route = gson.fromJson(routeString, Route.class);
                                Point startPoint = route.getStart();
                                Point endPoint = route.getDestination();
                                Point[] midPoints = route.getPoints();
                                //Add route markers to the map
                                IconFactory iconFactory =IconFactory.getInstance(OfflineMapActivity.this);
                                //Create starting marker
                                MarkerViewOptions startMarker = new MarkerViewOptions();
                                startMarker.position(new LatLng(startPoint.getLat(), startPoint.getLng()))
                                        .title(startPoint.getName())
                                        .snippet("Start")
                                        .icon(iconFactory.fromResource(R.drawable.map_marker_green));
                                mapboxMap.addMarker(startMarker);
                                //Creare intermendiate markers
                                for (int i = 0; i < midPoints.length; i++) {
                                    MarkerViewOptions marker = new MarkerViewOptions();
                                    Point point = midPoints[i];
                                    marker.position(new LatLng(point.getLat(), point.getLng()))
                                            .snippet(point.getType())
                                            .title(point.getName());

                                    if (point.getType().equals("Resting place"))
                                        marker.icon(iconFactory.fromResource(R.drawable.map_marker_blue));
                                    else
                                        marker.icon(iconFactory.fromResource(R.drawable.map_marker_yellow));
                                    mapboxMap.addMarker(marker);
                                }
                                //Create final marker
                                MarkerViewOptions endMarker = new MarkerViewOptions();
                                endMarker.position(new LatLng(endPoint.getLat(), endPoint.getLng()))
                                        .title(endPoint.getName())
                                        .snippet("Destination")
                                        .icon(iconFactory.fromResource(R.drawable.map_marker_red));
                                mapboxMap.addMarker(endMarker);

                                //Get directions and build polyline
                                String directionsString = jsonObject.getString("directionsRoute");
                                generatedRoute = gson.fromJson(directionsString, DirectionsRoute.class);
                                PolylineOptions polyline = drawRoute(generatedRoute);
                                mapboxMap.addPolyline(polyline);

                                Toast.makeText(getApplicationContext(), "Success the directions route is " + generatedRoute.getWeight(), Toast.LENGTH_LONG).show();
                            } catch (Exception exception) {
                                Log.e("METADATA_ERROR", "Failed to decode metadata: " + exception.getMessage());
                                route = null;
                            }

                            //Set the camera to the region
                            // Get the region bounds and zoom and move the camera.
                            LatLngBounds bounds = ((OfflineTilePyramidRegionDefinition)
                                    offlineRegion.getDefinition()).getBounds();
                            double regionZoom = ((OfflineTilePyramidRegionDefinition)
                                    offlineRegion.getDefinition()).getMinZoom();

                            // Create new camera position
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(bounds.getCenter())
                                    .zoom(regionZoom)
                                    .build();

                            // Move camera to new position
                            mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Log.e("ERROR_LIST_OFFLINE", "onListError: " + error);
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    private PolylineOptions drawRoute(DirectionsRoute route) {
        // Convert LineString coordinates into LatLng[]
        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        LatLng points[] = new LatLng[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(
                    coordinates.get(i).getLatitude() / 10,
                    coordinates.get(i).getLongitude() / 10);
            Log.d("OFFLINE COORDINATES " + i, "lat long: " + coordinates.get(i).toString());
        }

        // Draw Points on MapView
        return new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#3887be"))
                .width(5);

    }
}
