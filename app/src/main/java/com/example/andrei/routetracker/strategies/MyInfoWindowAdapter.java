package com.example.andrei.routetracker.strategies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andrei.routetracker.R;
import com.example.andrei.routetracker.models.RTMarker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrei on 6/15/2017.
 */

public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    // These are both viewgroups containing an ImageView with id "badge" and two TextViews with id
    // "title" and "snippet".
    private final View mContents;
    private ArrayList<RTMarker> rtMarkers;

    public MyInfoWindowAdapter(Context context, ArrayList<RTMarker> rtMarkers) {
        mContents = LayoutInflater.from(context).inflate(R.layout.my_info_contents, null);
        this.rtMarkers = rtMarkers;
        Log.d("INFO_CONSTR", "Constructor has been called with markers nr " + rtMarkers.size());
    }


    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        if (marker.getTag() != null) {
            if (marker.getTag().getClass() == Integer.class) {
                //este marker RT
                int markerPos = (int) marker.getTag();
                render(rtMarkers.get(markerPos), marker, mContents);
                Log.d("INFO_CONT_FIN", "Finished render returning content " + mContents.toString());
                return mContents;
            }
        }
        return null;
    }

    private void render(RTMarker rtMarker, Marker marker, View view) {
        //Check if photo exists
        if (rtMarker.getImagePath() != null) {
            //Get the image bitmap
            Bitmap photo = BitmapFactory.decodeFile(rtMarker.getImagePath());
            try {
                photo = ImageManipulation.fixOrientation(photo, rtMarker.getImagePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((ImageView) view.findViewById(R.id.iv_info_window)).setImageBitmap(photo);
        } else {
            view.findViewById(R.id.iv_info_window).setVisibility(View.INVISIBLE);
        }
        TextView tvMessage = ((TextView) view.findViewById(R.id.tv_msg_info_window));
        if (rtMarker.getMessage() != null) {
            tvMessage.setText(rtMarker.getMessage());
        } else {
            tvMessage.setText("");
        }
        //TODO: Pun data la care a fost facuta poza desi consuma resurse
        TextView tvDate = ((TextView) view.findViewById(R.id.tv_date_info_window));
        //String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",Locale.getDefault()).format(new Date(new File(rtMarker.getImagePath()).lastModified()));
        //tvDate.setText(timeStamp);
        tvDate.setVisibility(View.INVISIBLE);
    }
}

