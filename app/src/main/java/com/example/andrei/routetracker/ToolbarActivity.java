package com.example.andrei.routetracker;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.andrei.routetracker.strategies.LoginStrategy;

public class ToolbarActivity extends AppCompatActivity {

    private boolean activeToolbar = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        DrawerLayout drawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_toolbar, null);
        FrameLayout frameLayout = (FrameLayout) drawerLayout.findViewById(R.id.activity_content);

        getLayoutInflater().inflate(layoutResID, frameLayout, true);
        super.setContentView(drawerLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (activeToolbar) {
            String title;

            switch (layoutResID) {
                case R.layout.activity_add_marker:
                    title = "Add your marker";
                    break;
                case R.layout.activity_home:
                    title = "Home";
                    break;
                case R.layout.activity_map:
                    title = "Map";
                    break;
                case R.layout.activity_signin:
                    title = "Sign in";
                    break;
                default:
                    title = "Unrecognised activity";
                    break;
            }

            setSupportActionBar(toolbar);
            setTitle(title);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.action_login:
                if(MyGlobalApp.getCurrentUser()!=null){
                    MyGlobalApp.setCurrentUser(null);
                    LoginStrategy.getInstance().clearUser(getApplicationContext());
                }
                intent = new Intent(getApplicationContext(),SigninActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_offline:
                // User chose the "Offline" action, go to offline map
                MyGlobalApp.offlineMode = !item.isChecked();
                item.setChecked(MyGlobalApp.offlineMode);
                if (MyGlobalApp.offlineMode) {
                    item.setIcon(R.drawable.online_48);
                    intent = new Intent(getApplicationContext(),OfflineMapActivity.class);
                    startActivity(intent);

                } else {
                    item.setIcon(R.drawable.offline_48);
                    intent = new Intent(getApplicationContext(),BufferActivity.class);
                    startActivity(intent);
                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        //Offline btn set-up
        MenuItem offlineBtn = menu.findItem(R.id.action_offline);
        offlineBtn.setChecked(MyGlobalApp.offlineMode);
        if (MyGlobalApp.offlineMode) {
            offlineBtn.setIcon(R.drawable.online_48);
        } else {
            offlineBtn.setIcon(R.drawable.offline_48);
        }

        MenuItem loginBtn = menu.findItem(R.id.action_login);
        if(MyGlobalApp.getCurrentUser()!=null){
            loginBtn.setTitle(R.string.logout);
        }else{
            loginBtn.setTitle(R.string.login);
        }


        return super.onPrepareOptionsMenu(menu);
    }
}
