package com.example.andrei.routetracker.strategies;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.andrei.routetracker.models.User;

import org.bson.types.ObjectId;
import org.mindrot.jbcrypt.BCrypt;

import static com.example.andrei.routetracker.MyGlobalApp.MyPREFERENCES;

/**
 * Created by Andrei on 4/8/2017.
 */
//Clasa singleton
public class LoginStrategy {

    private static LoginStrategy strategy = new LoginStrategy();

    private LoginStrategy() {
    }

    public static LoginStrategy getInstance() {
        return strategy;
    }


    private SharedPreferences sharedpreferences;

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    public void rememberUser(User user, Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("storedUserId", user.getUserId().toString());
        editor.putString("storedUsername", user.getUsername());
        editor.putString("storedPassword", user.getPassword());
        editor.apply();

    }

    public void clearUser(Context ctx) {
        sharedpreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
    }

    public User login(User user) {
        //check if a user with username exists or not
        MongoStrategy mongoStrategy = MongoStrategy.getInstance();
        User found = mongoStrategy.findUser(user);

        //user does not exist, log the error
        if (found == null) {
            Log.i("Login problem", "user introduced not found(username incorect)");
            return null;
        } else {
            //user has been found, check password
            if (isValidPassword(found, user.getPassword())) {
                return found;
            } else {
                Log.i("Login problem", "user introduced not found(password incorect)");
                return null;
            }
        }
    }

    public ObjectId register(User user) {
        MongoStrategy mongoStrategy = MongoStrategy.getInstance();
        User found = mongoStrategy.findUser(user);

        if (found != null) {
            Log.i("Register problem", "username already exists");
            return null;
        } else {
            //if there is no user, create the user
            return mongoStrategy.createUser(user.getUsername(), createHash(user.getPassword()));
        }
    }

    //Check hashed password with real one. plaintext,hashed
    private boolean isValidPassword(User user, String plainPassword) {
        return BCrypt.checkpw(plainPassword, user.getPassword());
    }

    private String createHash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(10));
    }


};

