package com.example.andrei.routetracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.andrei.routetracker.models.RTMarker;
import com.example.andrei.routetracker.strategies.ImageManipulation;
import com.example.andrei.routetracker.strategies.MongoStrategy;
import com.google.android.gms.maps.model.LatLng;

import org.bson.types.ObjectId;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.R.attr.bitmap;
import static android.R.attr.data;

public class AddMarkerActivity extends ToolbarActivity {



    private static final int REQUEST_IMAGE_CAPTURE = 3;
    private String photoPath;
    private ImageView imageView;
    private ImageButton photoButton;
    private Button doneButton;
    private Bitmap photoTaken;
    private EditText textComment;
    private Location location;
    private ObjectId routeId;

    //Intent
    private void sendTakePhotoIntent() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageManipulation.createImageFile();
                photoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = Uri.fromFile(photoFile);
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePhotoIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
        else{
            Toast.makeText(this,"Could not find camera",Toast.LENGTH_LONG).show();
        }
    }




    private void addPhotoToGallery(String photoPath) {
        Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File photoFile = new File(photoPath);
        Uri contentUri = Uri.fromFile(photoFile);
        mediaScannerIntent.setData(contentUri);
        this.sendBroadcast(mediaScannerIntent);
    }

    //Do I take the full sized photo, or do I take the photo that is the max size I'm ever going to use. I choose the latter.
    private Bitmap getAndSetPhotoToImageView(String photoPath) {
        photoTaken = BitmapFactory.decodeFile(photoPath);
        try {
            photoTaken = ImageManipulation.fixOrientation(photoTaken,photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        addPhotoToGallery(photoPath);
        imageView.setImageBitmap(photoTaken);
        imageView.setVisibility(ImageView.VISIBLE);
        Log.d("PHOTO_TAKEN_SIZE","Size is: "+photoTaken.getByteCount()+"Byte count, "+photoTaken.getAllocationByteCount()+" Allocation bytes");
        return photoTaken;
    }






    //after image was taken
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            getAndSetPhotoToImageView(photoPath);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_marker);


        Intent intent = getIntent();
        routeId = new ObjectId(intent.getStringExtra("EXTRA_MARKER_ROUTE"));
        location = intent.getParcelableExtra("EXTRA_MARKER_LOCATION");



        imageView = (ImageView) findViewById(R.id.iv_add_marker);

        if (savedInstanceState != null) {
            photoPath = savedInstanceState.getString("PhotoURI");
            if(photoPath==null){
                Toast.makeText(this,"Error saving photo, please try again.",Toast.LENGTH_LONG).show();
            }
            else {
                getAndSetPhotoToImageView(photoPath);
            }
        }
        else{
            imageView.setVisibility(ImageView.INVISIBLE);
        }

        doneButton = (Button) findViewById(R.id.btn_marker_done);
        photoButton = (ImageButton) findViewById(R.id.btn_marker_photo);
        textComment = (EditText) findViewById(R.id.et_comment);


        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendTakePhotoIntent();

            }
        });



        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = textComment.getText().toString();
                if(!comment.isEmpty() || photoTaken!=null){
                    //create a Marker object
                    RTMarker newMarker = new RTMarker(null,new LatLng(location.getLatitude(),location.getLongitude()),MyGlobalApp.getCurrentUser().getUserId(),routeId,comment);
                    //redundant
                    if(photoTaken!=null){
                        newMarker.setImagePath(photoPath);
                    }
                    Intent intent = new Intent();
                    intent.putExtra("EXTRA_MARKER",newMarker);
                    setResult(RESULT_OK,intent);
                    finish();

                }
                else{
                    Toast.makeText(view.getContext(),"There must either be a description or a photo",Toast.LENGTH_LONG).show();
                }


            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("ACTIVITY_SAVE_INSTANCE","Working");
        if(photoPath!=null) {
            Log.d("ACTIVITY_SAVE_INSTANCE","Not null... saving.");
            outState.putString("PhotoURI",photoPath);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.d("ACTIVITY_RESTORED","Is photo taken null? ");


    }
}
