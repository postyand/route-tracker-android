package com.example.andrei.routetracker;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.andrei.routetracker.models.Point;
import com.example.andrei.routetracker.models.RTMarker;
import com.example.andrei.routetracker.models.Route;
import com.example.andrei.routetracker.strategies.MongoStrategy;
import com.example.andrei.routetracker.strategies.MyInfoWindowAdapter;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;
import com.mapbox.services.Constants;
import com.mapbox.services.api.directions.v5.DirectionsCriteria;
import com.mapbox.services.api.directions.v5.MapboxDirections;
import com.mapbox.services.api.directions.v5.models.DirectionsResponse;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Response;


public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationUpdatesCallback;
    private CheckBox cbLocationUpdate;
    private Button btnMarkLocation;
    private Button btnSaveMap;
    private DialogInterface.OnClickListener dialogAlertLastLocationListener;
    private ProgressBar progressBar;

    private Location lastLocation;
    private Point startPoint;
    private ArrayList<Point> markerPoints = new ArrayList<>();
    private Point endPoint;
    private DirectionsRoute generatedRoute;
    private PolylineOptions routePolyline;
    private ArrayList<RTMarker> markersList = new ArrayList<>();
    private Route route;
    private AsyncTask createMarkerTask;
    private RTMarker newMarker;
    //Offline map saving variables
    private com.mapbox.mapboxsdk.geometry.LatLngBounds mapboxbounds;
    private OfflineManager offlineManager;
    private boolean isDownloadFinished;

    private static final int REQUEST_CODE_ADD_MARKER = 7;
    // JSON encoding/decoding
    public static final String JSON_CHARSET = "UTF-8";
    public static final String JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //Get widgets
        cbLocationUpdate = (CheckBox) findViewById(R.id.cb_location_update);
        btnMarkLocation = (Button) findViewById(R.id.btn_mark_location);
        if(MyGlobalApp.getCurrentUser()==null){
            btnMarkLocation.setVisibility(View.GONE);
        }
        btnSaveMap = (Button) findViewById(R.id.btn_save_map);
        //Set mapbox access token
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        // Set up the OfflineManager
        offlineManager = OfflineManager.getInstance(MapActivity.this);


        //Create alert box on click listener
        dialogAlertLastLocationListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        // 1000 meters
                        if (lastLocation == null) {
                            Toast.makeText(MapActivity.this, "No last location recorded", Toast.LENGTH_LONG).show();
                        } else {
                            double range = calculateMaxRangeFromRoute(lastLocation, 1000);
                            if (range == -1) {
                                Toast.makeText(MapActivity.this, "You are not in range( 1km ) of the route!", Toast.LENGTH_LONG).show();
                            } else if (range >= 0) {
                                Toast.makeText(MapActivity.this, "You are in range. Range: " + range, Toast.LENGTH_LONG).show();
                                //TODO: Make intent send to form activity and the form should save data on the route object and on the database. Also reset route or add the changes to the route when it returns!
                                sendIntentToAddMarker(lastLocation, route);
                            }
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        //Do nothing?
                        break;
                }
            }
        };

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Location API
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //callback for location update
        mLocationUpdatesCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                lastLocation = locationResult.getLastLocation();
                for (Location location : locationResult.getLocations()) {
                    Toast.makeText(MapActivity.this, "Location update nr" + locationResult.getLocations().size() + ": " + location.getLongitude() + " " + location.getLatitude(), Toast.LENGTH_SHORT).show();
                }

            }

            ;
        };

        Intent intent = getIntent();
        route = intent.getParcelableExtra("EXTRA_SELECTED_ROUTE");

        startPoint = route.getStart();
        if (route.getPoints() != null) {
            for (Point p : route.getPoints()) {
                markerPoints.add(p);
            }
        }
        endPoint = route.getDestination();
        Log.d("Map-Route", "Route start " + startPoint.getLat() + " : " + startPoint.getLng() + " Route end " + endPoint.getLat() + " : " + endPoint.getLng());

        //Get markers
        //TODO: Daca trebuie descarcata poza clientul o sa astepte destul de mult, ar trebui sa pun un loading. Ar trebui sa pun un loading screen.

        //GET THE rt markers
        markersList = MongoStrategy.getInstance().getMarkers(route.getRouteId()
                , MyGlobalApp.getCurrentUser().getUserId(), this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        String labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int labelIndex = 0;


        //Location service
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //sets the button to find current location to true
            mMap.setMyLocationEnabled(true);
            //gets location at start
            getCurrentLocation(mLocationUpdatesCallback);
        } else {
            throw new RuntimeException();
        }


        //Set the RT markers
        if (markersList != null) {
            Log.d("MAP_MARKERS", "They are in nr of " + markersList.size());
            for (int i = 0; i < markersList.size(); i++) {
                addMarkerToMap(markersList.get(i), i);
            }
        } else {
            Toast.makeText(this, "Marker list null. Execution fail.Try again!", Toast.LENGTH_LONG).show();
        }

        //See if there is a new marker to add from the add marker activity
        if (newMarker != null) {
            addMarkerToMap(newMarker, markersList.size() - 1);
            newMarker = null;
        }

        //Builder for LatLngBounds which helps with zooming in on the route
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();


        //Create starting marker
        MarkerOptions startMarker = new MarkerOptions();
        startMarker.position(new LatLng(startPoint.getLat(), startPoint.getLng()))
                .title(startPoint.getName())
                .snippet("Start")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(startMarker);
        //Add starting marker to the bounds builder
        boundsBuilder.include(startMarker.getPosition());

        //Creare intermendiate markers
        for (int i = 0; i < markerPoints.size(); i++) {
            MarkerOptions marker = new MarkerOptions();
            Point point = markerPoints.get(i);
            marker.position(new LatLng(point.getLat(), point.getLng()))
                    .snippet(point.getType())
                    .title(point.getName());

            if (point.getType().equals("Resting place"))
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            else
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            mMap.addMarker(marker);
            //Add marker to the bounds builder
            boundsBuilder.include(marker.getPosition());
        }

        //Create final marker
        MarkerOptions endMarker = new MarkerOptions();
        endMarker.position(new LatLng(endPoint.getLat(), endPoint.getLng()))
                .title(endPoint.getName())
                .snippet("Destination")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mMap.addMarker(endMarker);
        //Add final marker to the builder
        boundsBuilder.include(endMarker.getPosition());
        //Build the bounds
        LatLngBounds bounds = boundsBuilder.build();


        //20 padding
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                getResources().getDisplayMetrics().widthPixels,
                getResources().getDisplayMetrics().heightPixels,
                20));


        //Mapbox routing code
        Position startW = Position.fromCoordinates(startPoint.getLng(), startPoint.getLat());
        ArrayList<Position> intermediateW = new ArrayList<>();
        for (int i = 0; i < markerPoints.size(); i++) {
            intermediateW.add(Position.fromCoordinates(markerPoints.get(i).getLng(), markerPoints.get(i).getLat()));
        }
        Position endW = Position.fromCoordinates(endPoint.getLng(), endPoint.getLat());

        ArrayList<Position> allWaypoints = new ArrayList<>();
        allWaypoints.add(startW);
        allWaypoints.addAll(intermediateW);
        allWaypoints.add(endW);
        //Make directions/routing builder
        MapboxDirections.Builder directionsBuilder = new MapboxDirections.Builder()
                .setAccessToken("pk.eyJ1IjoicGFua2VyIiwiYSI6ImNqMDVlMmw2YjBpMm8yeG9sNTE1ODU3emkifQ.Hmvna4IL95n_wMp75fCJQA")
                .setProfile(DirectionsCriteria.PROFILE_CYCLING)
                .setCoordinates(allWaypoints);

        //Send builder to asynctask
        MapboxRouter router = new MapboxRouter();
        Response<DirectionsResponse> response = null;

        //Wait for the asynctask to finish and pass the response so we can draw the lines(consider putting a loading screen if it takes too long)
        try {
            response = router.execute(directionsBuilder).get();
            generatedRoute = response.body().getRoutes().get(0);
            double distance = generatedRoute.getDistance();

            //Get the polyline for future use
            routePolyline = drawRoute(generatedRoute);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        //Set widget behaviour
        cbLocationUpdate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    startLocationUpdates();
                } else {
                    stopLocationUpdates();
                }
            }
        });

        btnMarkLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Location oldLocation = lastLocation;
                if (createMarkerTask != null && createMarkerTask.getStatus() != AsyncTask.Status.FINISHED) {
                    Toast.makeText(view.getContext(), "Please wait for the current marker to finish uploading", Toast.LENGTH_LONG).show();
                    return;
                }

                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        if (!locationResult.getLocations().isEmpty()) {
                            // 1000 meters
                            lastLocation = locationResult.getLocations().get(0);
                            double range = calculateMaxRangeFromRoute(lastLocation, 1000);
                            if (range == -1) {
                                Toast.makeText(view.getContext(), "You are not in range( 1km ) of the route!", Toast.LENGTH_LONG).show();
                            } else if (range >= 0) {
                                Toast.makeText(view.getContext(), "You are in range. Range: " + range, Toast.LENGTH_LONG).show();
                                //TODO: Make intent send to form activity and the form should save data on the route object and on the database. Also reset route or add the changes to the route when it returns!
                                sendIntentToAddMarker(lastLocation, route);
                            }

                        } else {
                            Toast.makeText(view.getContext(), "Location list empty?!?!This shouldn't happen ever", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onLocationAvailability(LocationAvailability locationAvailability) {
                        super.onLocationAvailability(locationAvailability);

                    }
                });
            }
        });

        //TODO:Listener for save map with bounds
        btnSaveMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // delete the last item in the offlineRegions list if there is an item
                offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
                    @Override
                    public void onList(OfflineRegion[] offlineRegions) {
                        if (offlineRegions.length > 0) {
                            offlineRegions[(offlineRegions.length - 1)].delete(new OfflineRegion.OfflineRegionDeleteCallback() {
                                @Override
                                public void onDelete() {
                                    Toast.makeText(MapActivity.this, "Map deleted", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onError(String error) {
                                    Log.e("DELETE_SAVED_ERR", "On Delete error: " + error);
                                }
                            });
                        }
                    }
                    @Override
                    public void onError(String error) {
                        Log.e("ERROR_LIST_OFFLINE", "onListError: " + error);
                    }
                });

                // Define the offline region
                OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                        "mapbox://styles/mapbox/outdoors-v10",
                        mapboxbounds,
                        10,
                        20,
                        MapActivity.this.getResources().getDisplayMetrics().density);

                byte[] metadata;
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("route",
                            gson.toJson(route));
                    jsonObject.put("directionsRoute",
                            gson.toJson(generatedRoute));
                    String json = jsonObject.toString();
                    metadata = json.getBytes(JSON_CHARSET);
                } catch (Exception exception) {
                    Log.e("SAVE_MAP_ERROR", "Failed to encode metadata: " + exception.getMessage());
                    metadata = null;
                }

                // Create the region asynchronously
                offlineManager.createOfflineRegion(
                        definition,
                        metadata,
                        new OfflineManager.CreateOfflineRegionCallback() {
                            @Override
                            public void onCreate(OfflineRegion offlineRegion) {
                                offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);

                                // Display the download progress bar
                                progressBar = (ProgressBar) findViewById(R.id.progress_bar_map);
                                startProgress();

                                // Monitor the download progress using setObserver
                                offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
                                    @Override
                                    public void onStatusChanged(OfflineRegionStatus status) {

                                        // Calculate the download percentage and update the progress bar
                                        double percentage = status.getRequiredResourceCount() >= 0
                                                ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                                                0.0;

                                        if (status.isComplete()) {
                                            // Download complete
                                            endProgress();
                                        } else if (status.isRequiredResourceCountPrecise()) {
                                            // Switch to determinate state
                                            setPercentage((int) Math.round(percentage));
                                        }
                                    }

                                    @Override
                                    public void onError(OfflineRegionError error) {
                                        // If an error occurs, print to logcat
                                        Log.e("ERROR_SAVE_MAP", "onError reason: " + error.getReason() + " mesage: " + error.getMessage());
                                    }

                                    @Override
                                    public void mapboxTileCountLimitExceeded(long limit) {
                                        // Notify if offline region exceeds maximum tile count
                                        Log.e("ERROR_TOO_MANY", "Mapbox tile count limit exceeded: " + limit);
                                    }
                                });
                            }

                            @Override
                            public void onError(String error) {
                                Log.e("SIMPLE_SAVE_ERROR", "Error: " + error);
                            }
                        });

            }
        });


        //Set adapter for info window
        mMap.setInfoWindowAdapter(new

                MyInfoWindowAdapter(getApplicationContext(), markersList));
        mMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener()

        {
            @Override
            public void onInfoWindowLongClick(Marker marker) {
                if (marker.getTag() != null && marker.getTag().getClass() == Integer.class) {
                    //este marker RT
                    int markerPos = (int) marker.getTag();
                    RTMarker rtMarker = markersList.get(markerPos);

                    //TODO: Grija aici la logica!!!
                    Log.d("RTMARKER_NULL_ID", "This stupid null id marker is " + rtMarker.getMessage());
                    if (rtMarker.getMarkerId() == null) {
                        Toast.makeText(getApplicationContext(), "Cancelling upload!!", Toast.LENGTH_LONG).show();
                        createMarkerTask.cancel(true);
                    } else {
                        Log.d("REMOVE_BTN_CLICK", "You will remove from databse " + rtMarker.getMessage());
                        //delete from database.Does it take a big amount of time?
                        MongoStrategy.getInstance().deleteMarker(rtMarker);
                        //remove from list
                        markersList.remove(rtMarker);
                    }
                    //remove from map
                    marker.remove();
                }
            }
        });


    }

    private double calculateMaxRangeFromRoute(Location location, double maxRange) {
        //Use lastLocation and the polyline generated
        for (LatLng polyCoords : routePolyline.getPoints()) {
            float[] results = new float[1];
            Location.distanceBetween(lastLocation.getLatitude(), lastLocation.getLongitude(),
                    polyCoords.latitude, polyCoords.longitude, results);

            if (results[0] < maxRange) {
                return results[0];
            }
        }
        return -1;
    }

    private PolylineOptions drawRoute(DirectionsRoute route) {
        // Convert LineString coordinates into LatLng[]
        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        LatLng points[] = new LatLng[coordinates.size()];
        List<com.mapbox.mapboxsdk.geometry.LatLng> mapboxPoints = new ArrayList<>();
        Log.d("MAP COORDINATES size", " " + coordinates.size());
        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(
                    coordinates.get(i).getLatitude() / 10,
                    coordinates.get(i).getLongitude() / 10);
            //Let's try this
            mapboxPoints.add(new com.mapbox.mapboxsdk.geometry.LatLng(
                    coordinates.get(i).getLatitude()/10,
                    coordinates.get(i).getLongitude()/10
            ));
            Log.d("MAP COORDINATES " + i, "lat long: " + coordinates.get(i).toString());
        }

        mapboxbounds = new com.mapbox.mapboxsdk.geometry.LatLngBounds.Builder()
                .includes(mapboxPoints)
                .build();

        // Draw Points on MapView
        PolylineOptions routePolyline = new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#3887be"))
                .width(5);
        mMap.addPolyline(routePolyline);

        return routePolyline;
    }

    private class MapboxRouter extends AsyncTask<MapboxDirections.Builder, Integer, Response<DirectionsResponse>> {

        @Override
        protected Response<DirectionsResponse> doInBackground(MapboxDirections.Builder... mapboxBuilders) {
            MapboxDirections client = mapboxBuilders[0].build();

            Response<DirectionsResponse> response = null;
            try {
                response = client.executeCall();


            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

    }


    //method that starts location updates
    private void startLocationUpdates() {
        //create new request location obejct
        LocationRequest mLocationRequest = new LocationRequest()
                .setInterval(15000)
                .setFastestInterval(8000)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        handleLocationRequest(mLocationRequest, mLocationUpdatesCallback);

    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationUpdatesCallback);
    }

    //create a builder object for location requests which makes a location request
    private void handleLocationRequest(LocationRequest mLocationRequest, LocationCallback mLocationCallback) {
        LocationSettingsRequest.Builder locationBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient locationClient = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = locationClient.checkLocationSettings(locationBuilder.build());


        //Success or failure solvers
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
                if (ContextCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                            mLocationCallback,
                            null);
                } else {
                    throw new SecurityException();
                }
                Toast.makeText(MapActivity.this, "Listener : " + locationSettingsResponse.getLocationSettingsStates().toString(), Toast.LENGTH_LONG).show();
            }

        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (!mLocationCallback.equals(mLocationUpdatesCallback)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
                    builder.setMessage("Could not get current location.\n Would you like to use your last know location instead?").setPositiveButton("Yes", dialogAlertLastLocationListener)
                            .setNegativeButton("No", dialogAlertLastLocationListener).show();
                }
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(MapActivity.this, 0x1
                            );

                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        break;
                }
            }
        });


    }

    private void sendIntentToAddMarker(Location location, Route route) {
        Intent intent = new Intent(getApplicationContext(), AddMarkerActivity.class);
        intent.putExtra("EXTRA_MARKER_LOCATION", location);
        intent.putExtra("EXTRA_MARKER_ROUTE", route.getRouteId().toString());

        startActivityForResult(intent, REQUEST_CODE_ADD_MARKER);
    }


    //Get current location
    private void getCurrentLocation(LocationCallback mLocationCallback) {
        LocationRequest mLocationRequest = new LocationRequest()
                .setNumUpdates(1)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        handleLocationRequest(mLocationRequest, mLocationCallback);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (cbLocationUpdate.isChecked()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        //Cod care intrerupe upload daca se iese de pe activitate. In mod normal upload-ul poate continua si fara ctivitate
        /*if(createMarkerTask!=null && createMarkerTask.getStatus()!= AsyncTask.Status.FINISHED){
            Toast.makeText(getApplicationContext(),"Cancelling upload!!",Toast.LENGTH_LONG).show();
            createMarkerTask.cancel(true);
        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_MARKER) {

            Bundle extras = data.getExtras();
            if (extras != null) {
                newMarker = extras.getParcelable("EXTRA_MARKER");
                Log.d("EXTRAS_NOT_NULL", "Map activity has extras " + extras.toString());
                if (newMarker != null) {
                    //Adaug in baza de date newMarker
                    createMarkerTask = MongoStrategy.getInstance().createMarker(newMarker, this);
                    Log.d("MARKER_NOT_NULL", "newMarker not null " + newMarker.getMessage());
                    markersList.add(newMarker);
                    addMarkerToMap(newMarker, markersList.size() - 1);
                }

            } else {
                Toast.makeText(this, "No new changes made", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void addMarkerToMap(RTMarker marker, int poz) {
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions().position(marker.getLatLng())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                    .title(marker.getMessage()))
                    //the tag will be where in the list it is found
                    .setTag(poz);

            newMarker = null;
        }
    }

    private void startProgress() {

        // Start and show the progress bar
        isDownloadFinished = false;
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void setPercentage(final int percentage) {
        progressBar.setIndeterminate(false);
        progressBar.setProgress(percentage);
    }

    private void endProgress() {
        // Don't notify more than once
        if (isDownloadFinished) {
            return;
        }

        // Stop and hide the progress bar
        isDownloadFinished = true;
        progressBar.setIndeterminate(false);
        progressBar.setVisibility(View.GONE);

        // Show a toast
        Toast.makeText(this, "Map saved. Go offline to view.", Toast.LENGTH_LONG).show();
    }


}



