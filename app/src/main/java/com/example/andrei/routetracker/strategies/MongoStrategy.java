package com.example.andrei.routetracker.strategies;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrei.routetracker.AddMarkerActivity;
import com.example.andrei.routetracker.MyGlobalApp;
import com.example.andrei.routetracker.R;
import com.example.andrei.routetracker.SigninActivity;
import com.example.andrei.routetracker.models.Point;
import com.example.andrei.routetracker.models.RTMarker;
import com.example.andrei.routetracker.models.Route;
import com.example.andrei.routetracker.models.User;
import com.google.android.gms.maps.model.LatLng;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.MongoGridFSException;
import com.mongodb.ServerAddress;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSUploadStream;
import com.mongodb.client.gridfs.model.GridFSDownloadOptions;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.event.ServerClosedEvent;
import com.mongodb.event.ServerDescriptionChangedEvent;
import com.mongodb.event.ServerListener;
import com.mongodb.event.ServerOpeningEvent;
import com.mongodb.gridfs.GridFS;

import org.bson.BsonObjectId;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Andrei on 4/12/2017.
 */

//clasa singleton
public class MongoStrategy {


    private MongoDatabase db = null;
    private MongoClient mongoClient;
    private MongoStatusListener mongoStatusListener;
    private static MongoStrategy strategy = new MongoStrategy();


    //Clasa listener ca sa vad mesaje de eroare
    private class MongoStatusListener implements ServerListener {

        private boolean available = false;

        public boolean isAvailable() {
            return available;
        }

        @Override
        public void serverOpening(ServerOpeningEvent event) {
        }

        @Override
        public void serverClosed(ServerClosedEvent event) {
        }

        @Override
        public void serverDescriptionChanged(ServerDescriptionChangedEvent event) {

            if (event.getNewDescription().isOk()) {
                available = true;
            } else if (event.getNewDescription().getException() != null) {
                Log.i("Mongo serv descr change", "exception: " + event.getNewDescription().getException().getMessage());
                available = false;
            }
        }
    }

    private MongoClient getMongoClient() {

        Log.d("Get client ", "Entered");

        if (mongoClient != null) {
            Log.d("Get client null", "mongoCloient not null");
            return mongoClient;
        }
        MongoClientOptions.Builder optionsBuilder = new MongoClientOptions.Builder();
        mongoStatusListener = new MongoStatusListener();
        optionsBuilder.addServerListener(mongoStatusListener);

        Log.d("Get mongo client", "Is trying to get the mongo client");
        try {
            MongoClientURI uri = new MongoClientURI("mongodb://mobile:m0b1l3@ds155747.mlab.com:55747/cicloturism");
            this.mongoClient = new MongoClient(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mongoClient;
    }

    public boolean isAvailable() {
        return mongoStatusListener.isAvailable();
    }


    private MongoStrategy() {
        Log.d("Mongo constructor", "calls connect to database");
        this.connectToDatabase();
    }

    //use to reconnect to the database without freezing app
    public void connectToDatabaseAsync() {
        Log.d("Mongo connect async", "execute async task");
        new databaseConnection().execute();
    }

    //use in the constructor to instantiate the database
    private void connectToDatabase() {
        Log.d("Mongo connect sync", "execute sync task");
        Log.d("Mongo inBackground", "gets database " + getMongoClient().toString());
        db = getMongoClient().getDatabase("cicloturism");
    }

    public static MongoStrategy getInstance() {
        return strategy;
    }


    private class databaseConnection extends AsyncTask<Void, Void, MongoDatabase> {

        @Override
        protected MongoDatabase doInBackground(Void... voids) {
            Log.d("Mongo inBackground", "gets database " + getMongoClient().toString());
            return getMongoClient().getDatabase("cicloturism");
        }

        @Override
        protected void onPostExecute(MongoDatabase o) {

            db = o;
            Log.d("Mongo postExect", db.toString());
        }

    }


    //verifica daca gaseste user-ul in baza de date, returneaza null daca nu
    public User findUser(User user) {
        User found = null;
        try {
            found = new FindUserTask().execute(user).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return found;
    }

    private class FindUserTask extends AsyncTask<User, Integer, User> {
        @Override
        protected User doInBackground(User... users) {
            User found = null;

            MongoCollection<Document> collection = db.getCollection("users");
            BasicDBObject query = new BasicDBObject("username", users[0].getUsername());
            MongoIterable<Document> cursor = collection.find(query);

            Document bsonUser = cursor.first();
            if (bsonUser != null) {
                found = new User(bsonUser.getString("username"),
                        bsonUser.getString("password"),
                        bsonUser.getObjectId("_id"));
            }

            return found;
        }
    }

    public ObjectId createUser(String username, String hashedPassword) {

        ObjectId id = null;
        try {
            id = new CreateUserTask().execute(username, hashedPassword).get(10,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return id;
    }

    private class CreateUserTask extends AsyncTask<String, Integer, ObjectId> {

        @Override
        protected ObjectId doInBackground(String... strings) {
            MongoCollection<Document> collection = db.getCollection("users");

            ObjectId id = new ObjectId();

            Document userDocument = new Document("username", strings[0])
                    .append("password", strings[1])
                    .append("_id",id);


            collection.insertOne(userDocument);
            return id;
        }


    }

    public ArrayList<Route> getAllRoutes() {
        ArrayList<Route> results = new ArrayList<>();

        try {
            results = new GetAllRoutesTask().execute().get(30, TimeUnit.SECONDS);
            Log.d("GOT RESULTS NOW", results.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            Log.e("ASYNCTASK TIMEOUT", "takes too much to get ALL the routes");
            e.printStackTrace();
        }

        return results;
    }

    private class GetAllRoutesTask extends AsyncTask<Void, Integer, ArrayList<Route>> {


        private GetAllRoutesTask(){
        }

        @Override
        protected ArrayList<Route> doInBackground(Void... voids) {
            ArrayList<Route> routes = new ArrayList<>();
            MongoCollection<Document> collection = db.getCollection("routes");

            MongoCursor<Document> cursor = collection.find().iterator();
            try {
                while (cursor.hasNext()) {
                    Document bsonRoute = cursor.next();
                    Log.d("WTFYAYO before", bsonRoute.toJson());
                    Route route = new Route();
                    route.setRouteId(bsonRoute.getObjectId("_id"));
                    route.setUserId(bsonRoute.getObjectId("userId"));
                    //parsing JSON
                    JSONObject document = new JSONObject(bsonRoute.toJson());
                    route.setTitle(document.optString("title", "Untitled"));
                    route.setCost(document.optDouble("cost", -1));
                    route.setTime(document.optInt("time", -1));
                    route.setDescription(document.optString("description"));
                    //folosesc BSON aici doar
                    route.setCreated_at(bsonRoute.getDate("created_at"));
                    Log.d("DATA SETATA", "data e :" + bsonRoute.getDate("created_at"));


                    JSONArray pointsJSON = document.optJSONArray("points");
                    if (pointsJSON.length() != 0) {
                        Point[] points = new Point[pointsJSON.length()];
                        //length e 0 nu? fara if daca nu are
                        for (int i = 0; i < pointsJSON.length(); i++) {
                            JSONObject pointJSON = pointsJSON.getJSONObject(i);
                            JSONObject pointLatLng = pointJSON.getJSONObject("latLng");
                            Point point = new Point(pointJSON.getString("name"), pointLatLng.getDouble("lat"), pointLatLng.getDouble("lng"),
                                    pointJSON.getString("type"), pointJSON.optString("description"));
                            points[i] = point;
                        }
                        route.setPoints(points);
                    }

                    JSONObject startJSON = document.getJSONObject("start");
                    JSONObject startLatLng = startJSON.getJSONObject("latLng");
                    Point start = new Point(startJSON.optString("name"), startLatLng.getDouble("lat"), startLatLng.getDouble("lng"),
                            "start", startJSON.optString("description"));
                    route.setStart(start);

                    JSONObject finishJSON = document.getJSONObject("destination");
                    JSONObject finishLatLng = finishJSON.getJSONObject("latLng");
                    Point finish = new Point(finishJSON.optString("name"), finishLatLng.getDouble("lat"), finishLatLng.getDouble("lng"),
                            "destination", finishJSON.optString("description"));
                    route.setDestination(finish);

                    Log.d("WILL INSERT ROUTE", route.toString());
                    routes.add(route);
                    Log.d("INSERTED IN ROUTES", routes.toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }

            return routes;
        }
    }

    public AsyncTask createMarker(RTMarker marker, Activity activityContext) {
       return new CreateImageMarkerTask(activityContext).execute(marker);
    }

    private class CreateImageMarkerTask extends AsyncTask<RTMarker, Integer, Void> {

        Activity activityContext;
        ProgressBar progressBar;
        TextView progressText;

        private CreateImageMarkerTask(Activity activityContext){
            this.activityContext = activityContext;
            progressBar = (ProgressBar)activityContext.findViewById(R.id.progress_bar_map);
            progressText = (TextView)activityContext.findViewById(R.id.tv_progress);
            progressBar.setProgress(0);
            progressBar.setVisibility(View.VISIBLE);
            progressText.setText(R.string.upload_image_progress_message);
            progressText.setVisibility(View.VISIBLE);
        }


        @Override
        protected Void doInBackground(RTMarker... rtMarkers) {
            RTMarker newMarker = rtMarkers[0];
            Document markerDocument = new Document("lat", newMarker.getLatLng().latitude)
                    .append("lng", newMarker.getLatLng().longitude)
                    .append("userId", newMarker.getUserId())
                    .append("routeId", newMarker.getRouteId())
                    .append("message", newMarker.getMessage())
                    .append("imagePath",newMarker.getImagePath());
            Log.d("MONGO_MARKER_STARTED", "Process started for marker " + newMarker.toString());


            //check if image is null
            if (!newMarker.getImagePath().isEmpty()) {
                Log.d("MONGO_MARKER_IMAGE", "Marker has image file " + newMarker.getImagePath());
                InputStream inputStream = null;
                try {
                    File imageFile = new File(newMarker.getImagePath());
                    long fileSize = imageFile.length();
                    inputStream = new FileInputStream(imageFile);
                    GridFSBucket gridBucket = GridFSBuckets.create(db, "markers");


                    // Create some custom options
                    GridFSUploadOptions uploadOptions = new GridFSUploadOptions()
                            .chunkSizeBytes(1024)
                            .metadata(markerDocument);
                    //ObjectId fileId = gridBucket.uploadFromStream(imageFile.getName(), inputStream, uploadOptions);
                    ObjectId id = new ObjectId();
                    GridFSUploadStream uploadStream = gridBucket.openUploadStream(new BsonObjectId(id),imageFile.getName(), uploadOptions);
                    int chunkSize = 1024;
                    byte[] buffer = new byte[chunkSize];
                    try {
                        int len;
                        double progress=0;
                        while((len = inputStream.read(buffer)) != -1) {
                            if(isCancelled()){
                                uploadStream.abort();
                                break;
                            }
                            uploadStream.write(buffer, 0, len);
                            Log.d("UPLOAD_PROGR_VAL","Progress values are: len - "+len+" file size - "+fileSize);
                            progress+=((double)len*100 / fileSize);
                            Log.d("UPLOAD_PROGRESS","Porgress is: "+progress);
                            publishProgress((int) progress);
                        }

                        uploadStream.close();
                        //Set the new ID from null to this after it finished uploading
                        newMarker.setMarkerId(id);
                    } catch (IOException var10) {
                        uploadStream.abort();
                        throw new MongoGridFSException("IOException when reading from the InputStream", var10);
                    }


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {

                Log.d("MONGO_MARKER_NO_IMAGE", "Marker has no image " + newMarker.getMessage());
                MongoCollection<Document> markerCollection = db.getCollection("markers");
                markerCollection.insertOne(markerDocument);
            }

            Log.d("MONGO_MARKER_ENDED", "Process ended for marker " + newMarker.toString());

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.d("ON_PROGRESS_UPDATE","PorgressBar value is: "+values[0]);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.INVISIBLE);
            progressText.setVisibility(View.INVISIBLE);

        }
    }

    public ArrayList<RTMarker> getMarkers(ObjectId routeId, ObjectId userId, Activity activityContext){

        ArrayList<RTMarker> list = null;
        try {
            list = new GetMarkersTask(activityContext).execute(routeId,userId).get(100,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return list;
    }

    private class GetMarkersTask extends AsyncTask<ObjectId,Integer,ArrayList<RTMarker>>{

        Activity activityContext;
        ProgressBar progressBar;
        TextView progressText;

        private GetMarkersTask(Activity activityContext){
            this.activityContext = activityContext;
            progressBar = (ProgressBar)activityContext.findViewById(R.id.progress_bar_map);
            progressText = (TextView)activityContext.findViewById(R.id.tv_progress);
            progressBar.setProgress(0);
            progressBar.setVisibility(View.VISIBLE);
            progressText.setText(R.string.download_image_progress_message);
            progressText.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<RTMarker> doInBackground(ObjectId... objectIds) {
            GridFSBucket gridBucket = GridFSBuckets.create(db,"markers");

            ArrayList<RTMarker> markerList = new ArrayList<>();

            //create query. Might be wrong since routeId and userId is in metadata.
            //Doesnt seem to work
            BasicDBObject query = new BasicDBObject("metadata.routeId", objectIds[0])
                    .append("metadata.userId",objectIds[1]);

            Log.d("MONGO_GET_MARKER_START","query: routeId "+objectIds[0].toString()+"; userId "+objectIds[1].toString());
            //Log.d("MONGO_FIRST_BUCKET","First marker from bucket has msg: "+gridBucket.find().first().getMetadata().getString("message"));
            //Iterate trough what was found
            gridBucket.find(query).forEach(new Block<GridFSFile>() {
                @Override
                public void apply(GridFSFile gridFSFile) {
                    Document markerDoc = gridFSFile.getMetadata();
                    double fileSize =gridFSFile.getLength();
                    Log.d("MONGO_GET_MARKER_ITER",markerDoc.toJson());
                    //Atentie ID-ul nu se ia din metadata
                    RTMarker marker = new RTMarker(gridFSFile.getObjectId(),
                            new LatLng(markerDoc.getDouble("lat"),markerDoc.getDouble("lng")),
                            markerDoc.getObjectId("userId"),
                            markerDoc.getObjectId("routeId"),
                            markerDoc.getString("message"));
                    marker.setImagePath(markerDoc.getString("imagePath"));

                    if(marker.getImagePath().isEmpty()) {
                        markerList.add(marker);
                    }
                    else{
                        File imageFile = new File(marker.getImagePath());
                        //daca exista deja poza in memorie nu o mai descarcam
                        if(imageFile.exists()){
                            markerList.add(marker);
                        }
                        //daca nu descarcam o copie a pozei si o punem intr-un direct al proiectului
                        else{
                            try {
                                String fileName = imageFile.getName();
                                //creez fisierul si iau path
                                File photoFile = ImageManipulation.createImageFile();

                                //descarc fisierul
                                FileOutputStream fileOutputStream = null;
                                try {
                                    fileOutputStream = new FileOutputStream(photoFile);
                                    GridFSDownloadOptions options = new GridFSDownloadOptions();
                                    //gridBucket.downloadToStream(fileName,fileOutputStream,options);
                                    GridFSDownloadStream downloadStream = gridBucket.openDownloadStream(fileName,options);
                                    byte[] buffer = new byte[downloadStream.getGridFSFile().getChunkSize()];
                                    MongoGridFSException savedThrowable = null;

                                    try {
                                        int len;
                                        double progress=0;
                                        try {
                                            while((len = downloadStream.read(buffer)) != -1) {
                                                fileOutputStream.write(buffer, 0, len);
                                                Log.d("DOWNLOAD_PROGR_VAL","Progress values are: len - "+len+" file size - "+fileSize);
                                                progress+=((double)len*100 / fileSize);
                                                Log.d("DOWNLOAD_PROGRESS","Porgress is: "+progress);
                                                publishProgress((int) progress);
                                            }
                                        } catch (IOException var17) {
                                            savedThrowable = new MongoGridFSException("IOException when reading from the OutputStream", var17);
                                        } catch (Exception var18) {
                                            savedThrowable = new MongoGridFSException("Unexpected Exception when reading GridFS and writing to the Stream", var18);
                                        }
                                    } finally {
                                        try {
                                            downloadStream.close();
                                        } catch (Exception var16) {
                                           ;;;;
                                        }

                                        if(savedThrowable != null) {
                                            throw savedThrowable;
                                        }

                                    }
                                    fileOutputStream.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }

                                //setez noul path
                                marker.setImagePath(photoFile.getAbsolutePath());

                                markerList.add(marker);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            });
            return markerList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.d("DWNL_PROGRESS_UPDATE","PorgressBar value is: "+values[0]);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<RTMarker> rtMarkers) {
            super.onPostExecute(rtMarkers);
            progressBar.setVisibility(View.INVISIBLE);
            progressText.setVisibility(View.INVISIBLE);
        }
    }


    public void deleteMarker(RTMarker marker){
        new DeteleMarkerTask().execute(marker);
    }

    private class DeteleMarkerTask extends AsyncTask<RTMarker,Integer,Void>{

        @Override
        protected Void doInBackground(RTMarker... rtMarkers) {
            RTMarker marker = rtMarkers[0];
            GridFSBucket gridBucket = GridFSBuckets.create(db,"markers");
            gridBucket.delete(marker.getMarkerId());
            return null;
        }
    }
}
