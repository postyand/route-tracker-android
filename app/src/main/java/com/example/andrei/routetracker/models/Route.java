package com.example.andrei.routetracker.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by Andrei on 4/12/2017.
 */

public class Route implements Parcelable{
    private ObjectId routeId;
    private Point start;
    private Point destination;
    private Point[] points;
    private String title;
    private double cost; //Euro
    private String elevation;
    private int time; //Hours
    private String description; //TODO: Documentele BSON au un storage f mare deci e putin probabil ca o sa treaca de limta. Totusi ar trebui sa setez o limita mai mica pe frontend de caractere la descriere.
    private Date created_at;
    private ObjectId userId;


    public Route(){

    };

    public Route(ObjectId routeId,Point start, Point destination, String title, Date created_at, ObjectId userId){
        this.start = start;
        this.destination = destination;
        this.title = title;
        this.created_at = created_at;
        this.userId = userId;
        this.routeId =routeId;
    };

    //Setters

    public void setStart(Point start) {
        this.start = start;
    }

    public void setDestination(Point destination) {
        this.destination = destination;
    }

    public void setPoints(Point[] points) {
        this.points = points;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setElevation(String elevation) {
        this.elevation = elevation;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public void setRouteId(ObjectId routeId){ this.routeId = routeId; }

    //Getters

    public Point getStart() {
        return start;
    }

    public Point getDestination() {
        return destination;
    }

    public Point[] getPoints() {
        return points;
    }

    public String getTitle() {
        return title;
    }

    public double getCost() {
        return cost;
    }

    public String getElevation() {
        return elevation;
    }

    public int getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public ObjectId getRouteId() { return routeId; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(start,i);
        Log.d("Parcel-Start","Destination be: "+start.getLat()+" : "+start.getLng());
        parcel.writeParcelable(destination,i);
        Log.d("Parcel-Dest","Destination be: "+destination.getLat()+" : "+destination.getLng());
        parcel.writeTypedArray(points, i);
        parcel.writeString(title);
        parcel.writeDouble(cost);
        parcel.writeString(elevation);
        parcel.writeInt(time);
        parcel.writeString(description);
        parcel.writeString(routeId.toString());
        if(created_at!=null) {
            parcel.writeLong(created_at.getTime());
        }
        else{
            parcel.writeLong(0);
        }
        parcel.writeString(userId.toString());

    }

    private Route(Parcel in) {
        start = in.readParcelable(Point.class.getClassLoader());
        Log.d("Unparcel-Start","Destination be: "+start.getLat()+" : "+start.getLng());
        destination = in.readParcelable(Point.class.getClassLoader());
        Log.d("Unarcel-Dest","Destination be: "+destination.getLat()+" : "+destination.getLng());
        points = in.createTypedArray(Point.CREATOR);
        title = in.readString();
        cost = in.readDouble();
        elevation = in.readString();
        time = in.readInt();
        description = in.readString();
        //se pierde string....
        routeId = new ObjectId(in.readString());
        created_at = new Date(in.readLong());
        userId = new ObjectId(in.readString());

    }

    public static final Parcelable.Creator<Route> CREATOR
            = new Parcelable.Creator<Route>() {
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
}
