package com.example.andrei.routetracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.andrei.routetracker.models.User;
import com.example.andrei.routetracker.strategies.LoginStrategy;

import org.bson.types.ObjectId;

public class SigninActivity extends ToolbarActivity {

    Button btnLogin, btnRegister, btnSkip;
    EditText etUsername, etPassword;
    CheckBox cbRemember;


    //poate extind aici
    private void checkRememberMe(User user) {
        if (cbRemember.isChecked()) {
            LoginStrategy.getInstance().rememberUser(user, getApplicationContext());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        btnLogin = (Button) findViewById(R.id.btnSignin);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnSkip = (Button) findViewById(R.id.btnSkipSignin);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);

        cbRemember = (CheckBox) findViewById(R.id.cbRememberMe);

        final MyGlobalApp myGlobalApp = ((MyGlobalApp) getApplicationContext());
        final Intent intent = new Intent(getApplicationContext(), HomeActivity.class);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User typedUser = new User(etUsername.getText().toString(), etPassword.getText().toString(),null);
                User fullUser = LoginStrategy.getInstance().login(typedUser);
                if ( fullUser == null) {
                    Toast.makeText(getApplicationContext(), "Incorrect username or password", Toast.LENGTH_LONG).show();
                } else {
                    checkRememberMe(fullUser);
                    MyGlobalApp.setCurrentUser(fullUser);
                    startActivity(intent);
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ok = true;
                String givenUsername = etUsername.getText().toString();
                String givenPassword = etPassword.getText().toString();
                if (givenUsername.isEmpty() && givenPassword.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please insert a username and password", Toast.LENGTH_LONG).show();
                    ok = false;
                } else {
                    if (givenUsername.length() < 5 || givenUsername.length() > 20) {
                        Toast.makeText(getApplicationContext(), "Username has to be between 5 and 20 characters long", Toast.LENGTH_LONG).show();
                        ok = false;
                    }
                    if (givenPassword.length() < 5 || givenPassword.length() > 20) {
                        Toast.makeText(getApplicationContext(), "Password has to be between 5 and 20 characters long", Toast.LENGTH_LONG).show();
                        ok = false;
                    } else if (givenPassword.matches("[a-zA-Z0-9]")) {
                        Toast.makeText(getApplicationContext(), "Password must not contain special characters", Toast.LENGTH_LONG).show();
                        ok=false;
                    }
                }
                if (ok) {
                    User typedUser = new User(givenUsername, givenPassword, null);
                    ObjectId newId = LoginStrategy.getInstance().register(typedUser);
                    if ( newId == null) {
                        Toast.makeText(getApplicationContext(), "Username already taken/Connection issues", Toast.LENGTH_LONG).show();
                    } else {
                        typedUser.setUserId(newId);
                        checkRememberMe(typedUser);
                        MyGlobalApp.setCurrentUser(typedUser);
                        startActivity(intent);
                    }
                }
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });


    }
}
