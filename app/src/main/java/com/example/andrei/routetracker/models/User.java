package com.example.andrei.routetracker.models;

import org.bson.types.ObjectId;

/**
 * Created by Andrei on 4/8/2017.
 */

public class User {

    private ObjectId userId;



    private String username;
    private String password;

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword(){
        return password;
    }

    public User(String username, String password, ObjectId userId) {
        this.username = username;
        this.password = password;
        this.userId = userId;
    }
}
