package com.example.andrei.routetracker.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.bson.types.ObjectId;

import java.io.File;
import java.util.Date;

/**
 * Created by Andrei on 6/12/2017.
 */

public class RTMarker implements Parcelable  {
    private ObjectId markerId;
    private LatLng latLng;
    private ObjectId userId;
    private ObjectId routeId;
    private String message = "";
    private String imagePath = "";

    public RTMarker(ObjectId markerId, LatLng latLng, ObjectId userId, ObjectId routeId, String message) {
        this.markerId = markerId;
        this.latLng = latLng;
        this.userId = userId;
        this.routeId = routeId;
        this.message = message;
    }

    public ObjectId getMarkerId() {
        return markerId;
    }

    public void setMarkerId(ObjectId markerId) {
        this.markerId = markerId;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public ObjectId getRouteId() {
        return routeId;
    }

    public void setRouteId(ObjectId routeId) {
        this.routeId = routeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeParcelable(latLng, i);
        parcel.writeString(userId.toString());
        parcel.writeString(routeId.toString());
        parcel.writeString(message);
        parcel.writeString(imagePath);
    }


    private RTMarker(Parcel in) {
        latLng = in.readParcelable(LatLng.class.getClassLoader());
        userId = new ObjectId(in.readString());
        routeId = new ObjectId(in.readString());
        message = in.readString();
        imagePath = in.readString();


    }

    public static final Parcelable.Creator<RTMarker> CREATOR
            = new Parcelable.Creator<RTMarker>() {
        public RTMarker createFromParcel(Parcel in) {
            return new RTMarker(in);
        }

        public RTMarker[] newArray(int size) {
            return new RTMarker[size];
        }
    };
}
