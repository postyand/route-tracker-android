package com.example.andrei.routetracker.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrei on 4/12/2017.
 */

public class Point implements Parcelable {
    private String name;
    private double lat;
    private double lng;
    private String type;
    private String description;

    public Point(String name, double lat, double lng, String type, String description) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.description = description;
    }



    //Setter
    public void setName(String name) {
        this.name = name;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //Getters
    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeString(type);
        parcel.writeString(description);
    }

    public Point(Parcel in) {
        name=in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        type=in.readString();
        description=in.readString();
    }

    public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator<Point>(){
        public Point createFromParcel(Parcel in) {
            return new Point(in);
        }

        public Point[] newArray(int size) {
            return new Point[size];
        }
    };
}
